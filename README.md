Battleship AI Tournament
========
##Summary
Write your own artificial intelligence for the Battleship board game in the Python programming language. Play your bot against yourself, your friends, or bots that play randomly. Play in individual matches or tournaments. View results in log files and in a web browser.

##Setup
Run this command to see one match played between the two demo bots:

	python3 battleship.py match bots/ocd.py bots/random.py --verbose --wait=0.1

The "--verbose" option means it will print each turn to the console. The "--wait=0.1" means the simulation will wait 0.1 seconds between each move.

Run this command to see all command line options:

	python3 battleship.py -h

##Writing your own bot
You will want to copy one of the bots in the "bots" folder. Copy "bots/ocd.py".

###1) Change the name
Name your bot! Change the bot_name value. Also, set student_name to equal your real, full name.

###2) Place your ships
At the start of the game, you need to place your ships.

	def get_setup(self):

This function must return a list of ships. Each ship has 4 values. For example, here's one ship:

	(4,2,3,False)

This ship is length 4, at coordinate (2,3). Finally, its rotation is False, meaning it is sideways/horizontal. True means upwards/vertical.

In a standard game of Battleship, there are five ships. Their lengths are 5, 4, 3, 3, and 2. The board is ten by ten. So the minimum x coordinate is 0, and the maximum x coordinate is 9.

###3) Where to shoot?

	def get_move(self, hits, shots):

This function must return two integers: the x and y coordinate of where you want to shoot next.

"hits" and "shots" are 2 dimensional arrays. They have all the information about the Battleship board - where you have shot, and where you have hit something. For example:

	if hits[5][5]:
	    if shots[5][6]:
	        return 0,2
	    else:
	        return 5,6
	else:
	    return 0,0

This code first checks if we have hit anything at coordinate (5,5). If not, it will shoot at (0,0).

If however we HAVE hit at (5,5), it checks if we have shot at (5,6). If we have, shoot at (0,2). If we haven't, shoot at (5,6).