import random

from constants import *

class BattleshipBot():
    def __init__(self):
        self.bot_name="Commander Rando"
        self.student_name="Stuart Spence"
        
    def get_move(self, hits, shots):
        "returns a random move."
        return random.randint(0,BOARD.WIDTH-1),random.randint(0,BOARD.HEIGHT-1)
    
    def get_iteration_move(self, hits, shots):
        "iterates over shots systematically until it finds some move."
        for i in range(len(shots)):
            for j in range(len(shots[0])):
                if not shots[i][j]:
                    return i,j
        return 0,0
    
    def get_setup(self):
        "places ships somewhat randomly, all horizontal."
        unused_rows=[i for i in range(BOARD.WIDTH)]
        ships=[]
        for ship_length in BOARD.SHIP_LENGTHS:
            row=random.choice(unused_rows)
            unused_rows.remove(row)
            ship=(ship_length,random.randint(0,BOARD.WIDTH-ship_length),row,False)
            ships.append(ship)
        return ships
	



