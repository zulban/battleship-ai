from constants import *

class BattleshipBot():
	def __init__(self):
		self.bot_name="Commander Itero"
		self.student_name="Stuart Spence"
	
	def get_move(self, hits, shots):
		for j in range(BOARD.WIDTH):
			for i in range(BOARD.HEIGHT):
				if not shots[i][j]:
					return i,j
		return 0,0
	
	def get_setup(self):
		ships = [(5,2,2,False),
			(4,2,3,False),
			(3,2,4,False),
			(3,2,5,False),
			(2,7,6,True)]
		return ships